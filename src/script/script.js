const arr = document.querySelectorAll("div#gameField > div");

/*==================================================================================
ÁREA DA MÁQUINA ====================================================================
====================================================================================*/

//Randomizando de 0 à 2, preparando para leitura de arrays na escolha da máquina.
function randomize(){
    return Math.floor(Math.random() * 3);
}
randomize()

//Função que lê o array, e randomiza. O retorno é a escolha da máquina.
function machineChoice(){
    let storage = "";
    let result = "";
    for (i = 0; i < arr.length; i++){
        storage = randomize();
    }
    if (storage === 0){
        result = arr[0];
    }else if (storage === 1){
        result = arr[1];
    }else {
        result = arr[2];
    }
    return result;    
}
machineChoice()


/*==================================================================================
ÁREA DO JOGADOR ====================================================================
====================================================================================*/

//selecionando o elemento pedra do html
let pedra   = document.getElementById("rock");
let papel   = document.getElementById("paper");
let tesoura = document.getElementById("scissors")


//criando escutador de evento com função anônima para variável pedra, papel e tesoura.
pedra.addEventListener("click", function(){   
    let machine = machineChoice();
    console.log(machine)
    let result = "";

    if (pedra === machine){
        result = `EMPATE.`;
    }else if (machine === document.getElementById("scissors")){
        result = `VITÓRIA.`;
    }else {
        result = `DERROTA.`;
    }
    console.log(result)

    /*Acessando árvore do DOM*/

    let minhaDiv = document.getElementById("returnField");
    let para = document.createElement("p");
    let nodeResult = document.createTextNode(result);
    let keepPlayer = document.getElementById("keepPlayer");
    let keepMachine = document.getElementById("keepMachine");
    let nodePlayer = document.createTextNode("PEDRA \n");
    let nodeMachine = "";

    //Condicional que atribui os cards a suas novas posições, indicando a escolha do jogador e da máquina.
    if (machine === document.getElementById("rock")){
        nodeMachine = document.createTextNode(`\n PEDRA \n`);
    }else if (machine === document.getElementById("paper")){
        nodeMachine = document.createTextNode(`\n PAPEL \n`);
    }else if (machine === document.getElementById("scissors")){
        nodeMachine = document.createTextNode(`\n TESOURA \n`)
    }

    
    keepMachine.appendChild(nodeMachine);
    keepPlayer.appendChild(nodePlayer);
    para.appendChild(nodeResult);
    minhaDiv.appendChild(para);



}, false);

papel.addEventListener("click", function(){
    let machine = machineChoice();
    console.log(machine)
    let result = "";

    if (papel === machine){
        result = `EMPATE.`;
    }else if(machine === document.getElementById("rock")){
        result = `VITÓRIA.`;
    }else {
        result = `DERROTA.`;
    }
    console.log(result)
    
    /*Acessando árvore DOM*/

    let minhaDiv = document.getElementById("returnField");
    let para = document.createElement("p");
    let nodeResult = document.createTextNode(result);
    let keepPlayer = document.getElementById("keepPlayer");
    let keepMachine = document.getElementById("keepMachine");
    let nodePlayer = document.createTextNode("PAPEL \n");
    let nodeMachine = "";

    //Condicional que atribui os cards a suas novas posições, indicando a escolha do jogador e da máquina.
    if (machine === document.getElementById("rock")){
        nodeMachine = document.createTextNode(`\n PEDRA \n`);
    }else if (machine === document.getElementById("paper")){
        nodeMachine = document.createTextNode(`\n PAPEL \n`);
    }else if (machine === document.getElementById("scissors")){
        nodeMachine = document.createTextNode(`\n TESOURA \n`)
    }

    
    keepMachine.appendChild(nodeMachine);
    keepPlayer.appendChild(nodePlayer);
    para.appendChild(nodeResult);
    minhaDiv.appendChild(para);

}, false);

tesoura.addEventListener("click", function(){
    let machine = machineChoice();
    console.log(machine)
    let result = "";

    if (tesoura === machine){
        result = `EMPATE.`;
    }else if (machine === document.getElementById("paper")){
        result = `VITÓRIA.`;
    }else {
        result = `DERROTA.`;
    }
    console.log(result)
    
    /*Acessdando DOM*/
    let minhaDiv = document.getElementById("returnField");
    let para = document.createElement("p");
    let nodeResult = document.createTextNode(result);
    let keepPlayer = document.getElementById("keepPlayer");
    let keepMachine = document.getElementById("keepMachine");
    let nodePlayer = document.createTextNode("TESOURA \n");
    let nodeMachine = "";

    //Condicional que atribui os cards a suas novas posições, indicando a escolha do jogador e da máquina.
    if (machine === document.getElementById("rock")){
        nodeMachine = document.createTextNode(`\n PEDRA \n`);
    }else if (machine === document.getElementById("paper")){
        nodeMachine = document.createTextNode(`\n PAPEL \n`);
    }else if (machine === document.getElementById("scissors")){
        nodeMachine = document.createTextNode(`\n TESOURA \n`)
    }
    
    keepMachine.appendChild(nodeMachine);
    keepPlayer.appendChild(nodePlayer);
    para.appendChild(nodeResult);
    minhaDiv.appendChild(para);

}, false);


